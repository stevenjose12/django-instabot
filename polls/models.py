from django.db import models

class InstagramAccounts(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=191)
    password = models.CharField(max_length=191)

class InstagramPosts(models.Model):
    id = models.AutoField(primary_key=True)
    ig_account = models.ForeignKey(InstagramAccounts, on_delete=models.CASCADE)
    status = models.IntegerField(default=1)
    username = models.CharField(max_length=191)
    url = models.CharField(max_length=191, default=None)
    users_amount = models.IntegerField(default=1)
    follow_users = models.IntegerField(default=1)
    prev_text = models.CharField(max_length=191, default=None)
    like_post = models.IntegerField(default=1)