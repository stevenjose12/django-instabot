from __future__ import unicode_literals

import argparse
import os
import sys
import threading
import logging
import random
import time
import asyncio
from tqdm import tqdm
from instabot.bot.bot import Bot as botLibrary  # noqa: E402
from polls.models import InstagramAccounts, InstagramPosts
class Bot:
    def __init__(self):
        print('init bot')
    def start():
        print('>>: Bot > ', botLibrary)
        print('>>: bot file > function to repair')
        users = InstagramAccounts.objects.filter()
        print('>>: accounts > ', users)
        def comments(bot, user, posts):
            bot.logger.info('>>: function comment started >>>>>')
            try:
                bot.login(username=user.username, password=user.password)
                bot.logger.info('>>: login end >>><>>-----')
                for post in posts:
                    bot.logger.info('>>: userLogin', user.username)
                    media_id = bot.get_media_id_from_link(post.url)
                    user_id = bot.get_user_id_from_username(post.username)
                    followers = bot.get_user_followers(user_id, 1000)
                    users = random.sample(followers, post.users_amount)
                    usersString = []
                    for user in users:
                            # bot.logger.info(user)
                            username = bot.get_username_from_user_id(user)
                            usersString.append('@'+username)
                            bot.logger.info('>>: userName > ',username)
                            bot.logger.info('>>: usersString > ', usersString)
                            pass
                    prev = post.prev_text or ''
                    to_comment = prev.join(list)
                    if(bot.comment(media_id, to_comment)):
                        bot.logger.info(">>: commented success > user: ", userLogin[0])
                    # main(bot.logout, 7 * 60)
                    bot.logger.info('>>: SECOND SLEEP')
                    time.sleep(120)
                    bot.logout()
                pass
                bot.logger.info('>>: Before sleep')
                time.sleep(60 * 60)
            except Exception as err:
                bot.logger.info('>>: error > except > ', err)
        threads = []
        for user in users:
            posts = InstagramPosts.objects.filter(ig_account=user.id)
            print('posts > ', posts)
            print('>>: user > ', user.id)
            bot = botLibrary()
            t = threading.Thread(target=comments, args=(bot, user, posts,))
            t.start()
            threads.append(t)
        return 
    if __name__ == "__start__":
        start()