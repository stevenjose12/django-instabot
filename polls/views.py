from django.http import HttpResponse
import os
from .bot import Bot

def index(request):
    Bot.start()
    return HttpResponse("Hello, world. You're at the polls index.")